""" Periodically correlate states from services between consul and cachetHQ """
import json
import re
import configparser
from enum import Enum
from time import sleep
import requests

def get_list_cachet_service(url):
    """ List all cachet services """
    # Liste des composants dans CachetHQ
    components_raw = requests.get(url + '/api/v1/components')
    components = components_raw.json()

    service_list = []
    for data in components["data"]:
        my_tags = data["tags"]
        my_id_component = data["id"]
        my_list_consul = []
        for _key, service_tag in my_tags.items():
            my_list_consul.append(service_tag)
        service_list.append(Service(my_list_consul, my_id_component))
    return service_list


def get_list_consul_service(url):
    """ List all consul services """
    services_raw = requests.get(url + '/v1/catalog/services')
    services = services_raw.json().keys()
    my_list = []
    for key in services:
        my_list.append(key)
    services_consul_list = [service for service in my_list]
    return services_consul_list


def get_state_cachet_service(cachet_service_id, token, url):
    """ Retrieve state of a cachet service """
    headers = {"X-Cachet-Token": token, "Content-Type": "application/json"}
    url_get = url + 'api/v1/components/' + str(cachet_service_id)
    state_raw = requests.get(url_get, headers=headers)
    state = state_raw.json()["data"]["status"]
    return Etat(int(state))


def main_function(token, url_cachet, url_consul):
    """ Main function, retrieving sevrices from consul and cachet and then
    correlating their state and updating this state in cachet if necessary """

    services_consul_list = get_list_consul_service(url_consul)
    service_list = get_list_cachet_service(url_cachet)

    regex_optional = re.compile(".*:O")

    # Mise a jour des services CachetHQ s'ils existent dans consul (version current)
    for service in service_list:
        etat_service = Etat.OK
        for consul_service in service.list_service_tags_name:
            is_optional = True if regex_optional.match(consul_service) else False
            consul_service = consul_service.replace(":O", "")
            if consul_service not in services_consul_list:
                if is_optional and etat_service != Etat.Major:
                    etat_service = Etat.Minor
                else:
                    etat_service = Etat.Major
        old_state = get_state_cachet_service(service.id_component, token, url_cachet)
        if old_state != etat_service:
            if etat_service == Etat.OK:
                payload = {"status": "1"}
            elif etat_service == Etat.Performance:
                payload = {"status": "2"}
            elif etat_service == Etat.Minor:
                payload = {"status": "3"}
            elif etat_service == Etat.Major:
                payload = {"status": "4"}

            headers = {"X-Cachet-token": token,
                       "Content-Type": "application/json"}
            url = url_cachet + "api/v1/components/" + str(service.id_component)
            print(str(service.id_component) + " " + str(payload))
            requests.put(url, data=json.dumps(payload), headers=headers)


if __name__ == '__main__':

    class Etat(Enum):
        """ Simple Enum for state """
        OK = 1
        Performance = 2
        Minor = 3
        Major = 4

    class Service():
        """ Service class """
        def __init__(self, list_service_tags_name, id_component):
            self._list_service_tags_name = list_service_tags_name
            self._id_component = id_component

        @property
        def list_service_tags_name(self):
            """ list_service_tags_name getter """
            return self._list_service_tags_name

        @list_service_tags_name.setter
        def list_service_tags_name(self, list_service_tags_name):
            """ list_service_tags_name setter """
            self._list_service_tags_name = list_service_tags_name

        @property
        def id_component(self):
            """ id_component getter """
            return self.id_component

        @id_component.setter
        def id_component(self, id_component):
            """ id_component setter """
            self._id_component = id_component



    CONFIG = configparser.ConfigParser()
    CONFIG.read('config.cfg')
    if CONFIG['DEFAULT']:
        REFRESH = CONFIG['DEFAULT']['refresh']
        TOKEN = CONFIG['DEFAULT']['TOKEN']
        URL_CACHET = CONFIG['DEFAULT']['url_cachet']
        URL_CONSUL = CONFIG['DEFAULT']['url_consul']
    else:
        print("EXIT: erreur de configuration")
        exit(255)

    while True:
        main_function(TOKEN, URL_CACHET, URL_CONSUL)
        sleep(int(REFRESH))

# EOF
