FROM python:3-alpine3.6

ARG VCS_REF="3fc0de8"
ARG BUILD_DATE="2018-09-12T10:00:00Z"

LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://framagit.org/Katyucha/consul-to-cachetHQ.git" \
      org.label-schema.url="https://framagit.org/Katyucha/consul-to-cachetHQ" \
      org.label-schema.name="Consul to cachetHQ" \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.license="GPLv3" \
      org.label-schema.build-date=$BUILD_DATE

WORKDIR /app

COPY LICENSE main.py run.sh .

RUN pip install requests && \
    rm -rf ~/.cache/pip

CMD ["./run.sh"]
