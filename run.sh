#!/bin/sh

export REFRESH=${REFRESH:-10}
export TOKEN=${TOKEN:-}
export URL_CACHET=${URL_CACHET:-}
export URL_CONSUL=${URL_CACHET:-}

cat << EOF > config.cfg
[DEFAULT]
refresh = ${REFRESH}
TOKEN = ${TOKEN}
urlCachet = ${URL_CACHET}
urlConsul = ${URL_CONSUL}
EOF

python main.py
