# ConfigurationFile

TOKEN variable is the user's token given by CachetHQ

```
[DEFAULT]
TOKEN=V6JGKdgVpdVLhfrsZR6I
urlCachet = http://localhost/
urlConsul = http://localhost:8500/
refresh = 10 // in second
```

# Service setup in CachetHQ :

Add service consul name in Tag, separated by comma

Exemple : service1,service2

For optional service : add 
```
:O
```
suffix, like 
```
service1,service2,service3:O
```
# NixOs

```
nix-shell --pure -p python36Packages.requests  --run "python main.py"
```
